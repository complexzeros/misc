import glob
from os import rename
from subprocess import check_output

oldFileNames = glob.glob("*.flac")

for oldFileName in oldFileNames:
    tag = check_output(["metaflac","--show-tag=discnumber",oldFileName]).decode("UTF-8")
    discNumber = tag[len(tag) - 2]
    rename(oldFileName, "0" + discNumber + "." + oldFileName)
